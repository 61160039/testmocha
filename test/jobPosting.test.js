const assert = require('assert')


describe('JobPosting', function () {
  describe('checkEnableTime', function () {

    const { checkEnableTime } = require('../JobPosting')
    it('shoud return true when เวลาสมัครอยู่ในช่วงเวลาระหว่างเวลาเริ่มต้นและเวลาสิ้นสุด', function () {
      //Arrage
      const startTime = new Date(2021, 1, 31)
      const endTime = new Date(2021, 2, 5)
      const today = new Date(2021, 2, 3)
      const expectedResult = true;
      //Act
      const actualResult = checkEnableTime(startTime, endTime, today)
      //Assert
      assert.strictEqual(actualResult, expectedResult);
    })

    it('shoud return true when เวลาสมัครเท่ากับเวลาเริ่มต้น',function () {
      //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,1,31)
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today)
    //Assert
    assert.strictEqual(actualResult,expectedResult);
    })

    it('shoud return true when เวลาสมัครเท่ากับเวลาสิ้นสุด',function () {
      //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,2,5)
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today)
    //Assert
    assert.strictEqual(actualResult,expectedResult);
    })


    it('shoud return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น',function () {
      //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,1,25)
    const expectedResult = false;
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today)
    //Assert
    assert.strictEqual(actualResult,expectedResult);
    })

    it('shoud return false when เวลาสมัครอยู่หลังเวลาสิ้นสุด',function () {
      //Arrage
    const startTime = new Date(2021,1,31)
    const endTime = new Date(2021,2,5)
    const today = new Date(2021,2,15)
    const expectedResult = false;
    //Act
    const actualResult = checkEnableTime(startTime,endTime,today)
    //Assert
    assert.strictEqual(actualResult,expectedResult);
    })
  })
}) 